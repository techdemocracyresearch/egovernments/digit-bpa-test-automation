package steps.bpa;

import cucumber.api.PendingException;
import cucumber.api.java8.En;
import cucumber.api.java8.Pa;
import dataBaseFiles.DatabaseReader;
import entities.PaymentCredentials;
import pages.PermitApplicationPage;
import steps.BaseSteps;
import utils.ScenarioContext;

import java.sql.SQLException;

public class permitApplication extends BaseSteps implements En {
    public permitApplication() {
        And("^he apply for (.*) under Plan Scrutiny$", (String planScrutiny) -> {
            pageStore.get(PermitApplicationPage.class).clickOnPlanScrutiny(planScrutiny);
        });
        And("^he selects required city as \"([^\"]*)\"$", (String city) -> {
        pageStore.get(PermitApplicationPage.class).selectCity(city);
        });
        And("^he enters dcr details for \"([^\"]*)\"$", (String riskType) -> {
            pageStore.get(PermitApplicationPage.class).enterDCRDetails(riskType);
        });
        And("^he copies the scrutiny number$", () -> {
            String scrutinyNumber = pageStore.get(PermitApplicationPage.class).scrutinyNumber();
           scenarioContext.setScrutinyNumber(scrutinyNumber);
        });
        When("^he apply for (.*) building permit$", (String applicationType) -> {
            pageStore.get(PermitApplicationPage.class).clickOnNewConstruction(applicationType);
        });
        And("^he enters basic scrutiny details$", () -> {
            pageStore.get(PermitApplicationPage.class).basicScrutinyDetails(scenarioContext.getScrutinyNumber());
        });
        And("^he enters Block wise occupancy details$", () -> {
            pageStore.get(PermitApplicationPage.class).blockWiseOccupancy();
        });
        And("^he enters Applicant Details$", () -> {
            pageStore.get(PermitApplicationPage.class).applicantDetails();
        });
        And("^he selects boundary details$", () -> {
            pageStore.get(PermitApplicationPage.class).boundaryDetails();
        });
        And("^he enters plot details$", () -> {
            pageStore.get(PermitApplicationPage.class).plotDetails();
        });
        And("^he declares that above information provided is correct$", () -> {
            pageStore.get(PermitApplicationPage.class).architectDeclaration();
        });
        And("^he attach the required documents$", () -> {
            pageStore.get(PermitApplicationPage.class).attachDocuments();
        });
        And("^he attach the required NOC documnets$", () -> {
            pageStore.get(PermitApplicationPage.class).attachNOC();
        });
        And("^he send the application to citizen$", () -> {
            pageStore.get(PermitApplicationPage.class).sendToCitizen();
        });
        And("^he copy application number$", () -> {
            scenarioContext.setApplicationNumber(pageStore.get(PermitApplicationPage.class).copyApplicationNumber());
        });
        And("^he accepts the building plan application$", () -> {
           pageStore.get(PermitApplicationPage.class).citizenAccept();
        });
        And("^he submit the application$", () -> {
            pageStore.get(PermitApplicationPage.class).submitPermitApplication();
        });
        And("^he pay permit scrutiny fee for \"([^\"]*)\"$", (String bank) -> {
            PaymentCredentials paymentCredentials = null;
            try {
                paymentCredentials = new DatabaseReader().getPaymentCredentials(bank);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            pageStore.get(PermitApplicationPage.class).enterPaymentCredentials(paymentCredentials);
        });
        And("^he verifies collection receipt has been generated$", () -> {
            pageStore.get(PermitApplicationPage.class).verifyGenerateReceipt();
        });
        And("^he print the permit order$", () -> {
            pageStore.get(PermitApplicationPage.class).printPermitOrder();
        });
        And("^he closes the acknowledgement page$", () -> {
            pageStore.get(PermitApplicationPage.class).closeAcknowledgement();
        });
        And("^he initiate inspection appointment$", () -> {
            pageStore.get(PermitApplicationPage.class).initiateInspectionAppointment();
        });
        And("^he capture the inspection details$", () -> {
            pageStore.get(PermitApplicationPage.class).captureInspectionDetails();
        });
        And("^he initiate mandatory NOC$", () -> {
//            scenarioContext.setNOCApplocationNumbet(
 pageStore.get(PermitApplicationPage.class).initiateNOC();
// );
            System.out.println("NOC Application Number = "+scenarioContext.getNOCApplicationNumber());
        });
        And("^he attach the noc for fire$", () -> {
            pageStore.get(PermitApplicationPage.class).attachFireNOC();
        });
        And("^he update NOC status$", () -> {
            pageStore.get(PermitApplicationPage.class).updateNOCStatus();
        });
        And("^he selects the permit conditions$", () -> {
            pageStore.get(PermitApplicationPage.class).selectPermitCondition();
        });
        And("^choose to pay online$", () -> {
            pageStore.get(PermitApplicationPage.class).clickOnPayOnline();
        });
        And("^he generate permit order$", () -> {
            pageStore.get(PermitApplicationPage.class).generatePermitOrder();
        });
        And("^he click on Services Completed tab$", () -> {
            pageStore.get(PermitApplicationPage.class).servicesComplted();
        });
        And("^he copies the building permit number$", () -> {
           scenarioContext.setPermitNumber(pageStore.get(PermitApplicationPage.class).getPermitNumber());
            System.out.println("Permit Number"+scenarioContext.getPermitNumber());
        });
        And("^he enters applicant details for plan scrutiny$", () -> {
            pageStore.get(PermitApplicationPage.class).planScrutinyApplicantDetails();
        });
        And("^he submit plan scrutiny$", () -> {
            pageStore.get(PermitApplicationPage.class).submitPlanScrutiny();
        });
        And("^he enters permit number$", () -> {
            pageStore.get(PermitApplicationPage.class).enterPermitNumber(scenarioContext.getPermitNumber());
        });
        And("^he enters occupancy certificate scrutiny number$", () -> {
            pageStore.get(PermitApplicationPage.class).OCPlanScrutiny(scenarioContext.getScrutinyNumber());
        });
        And("^he submit oc application$", () -> {
            pageStore.get(PermitApplicationPage.class).submitOCApplication();
        });
        And("^he attach the required documents for OC$", () -> {
            pageStore.get(PermitApplicationPage.class).attachOCDocuments();
        });
        And("^he copies the Occupancy Application Number$", () -> {
            scenarioContext.setApplicationNumber(pageStore.get(PermitApplicationPage.class).copyOCApplicationNumber());
        });
        And("^he attach the required NOC documents for OC$", () -> {
            pageStore.get(PermitApplicationPage.class).attachOCNOCDocuments();
        });
        And("^he update OCNOC status$", () -> {
            pageStore.get(PermitApplicationPage.class).updateOCNOCStatus();
        });
        And("^he generate occupancy certificate$", () -> {
            pageStore.get(PermitApplicationPage.class).generateOC();
        });
        And("^he copies Inspection Application Number$", () -> {
           scenarioContext.setApplicationNumber(pageStore.get(PermitApplicationPage.class).copyInspectionAppNum());
            System.out.println("InspecAppNum = "+scenarioContext.getScrutinyNumber());
        });
        And("^he enters permit to initiate inspection$", () -> {
            pageStore.get(PermitApplicationPage.class).enterPermitNumberForInspection(scenarioContext.getPermitNumber());
        });
        And("^he selects the rejection reason$", () -> {
            pageStore.get(PermitApplicationPage.class).rejectionReason();
        });
        And("^he generate the rejection notice$", () -> {
            pageStore.get(PermitApplicationPage.class).generateRejectionNotice();
        });

    }
}
