package steps;

import cucumber.api.PendingException;
import cucumber.api.java8.En;
import dataBaseFiles.DatabaseReader;
import entities.LoginDetails;
import pages.LandingScreenPages;

import java.sql.SQLException;

public class LandingScreenSteps extends BaseSteps implements En {
    public LandingScreenSteps() {
        Given("^(.*) logs in$", (String user) -> {
            LoginDetails loginDetails = null;
            try {
                loginDetails = new DatabaseReader().getLoginDetails(user);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        pageStore.get(LandingScreenPages.class).login(loginDetails);
        });
        And("^current user logs out$", () -> {
            pageStore.get(LandingScreenPages.class).logout();
        });
        And("^he opens the application from his inbox$", () -> {
            pageStore.get(LandingScreenPages.class).openApplicationInCitizenInbox(scenarioContext.getApplicationNumber());
        });
        And("^he choose to act upon the above application$", () -> {
            pageStore.get(LandingScreenPages.class).openApplicationInEmployeeInbox(scenarioContext.getApplicationNumber());
        });
        And("^current employee logs out$", () -> {
            pageStore.get(LandingScreenPages.class).employeeLogout();
        });
        And("^he choose to act upon the inspection application$", () -> {
            pageStore.get(LandingScreenPages.class).openInspectionApplicationInEmployee(scenarioContext.getApplicationNumber());
        });
    }
}
