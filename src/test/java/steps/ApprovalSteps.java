package steps;

import cucumber.api.PendingException;
import cucumber.api.java8.En;
import entities.ApprovalDetails;
import excelDataReader.ExcelDataReader;
import org.junit.Assert;
import pages.ApprovalPage;

public class ApprovalSteps extends BaseSteps implements En {
    public ApprovalSteps() {
        And("^he forward the application to \"([^\"]*)\"$", (String approverPosition) -> {
//            ApprovalDetails approvalDetails = new ExcelDataReader().getApprovalDetails(approverPosition);
            scenarioContext.setSuccessMsessage(pageStore.get(ApprovalPage.class).getApprover());
//            Assert.assertTrue(scenarioContext.getSuccessMessage().contains("successfully"));
        });
        And("^he approve noc application$", () -> {
           pageStore.get(ApprovalPage.class).approveNOC();
        });
        And("^he approve the application$", () -> {
            pageStore.get(ApprovalPage.class).approve();
        });
        And("^he rejects the application$", () -> {
            pageStore.get(ApprovalPage.class).reject();
        });
        And("^he revert the application$", () -> {
            pageStore.get(ApprovalPage.class).revert();
        });
    }
}
