package excelDataReader;

import builders.ApproverDetailsBuilder;
import entities.ApprovalDetails;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class ExcelDataReader {
    Workbook workbook;
    Sheet approverDetailsSheet;

    String permitApplicationExcel = "excelDataFile/";
    String approverDataExcel = "excelDataFile/approverData.xlsx";

    public void createWorkbook(String excelFileName){
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(excelFileName);
        try {
            workbook = WorkbookFactory.create(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    private int getCellNumberWithHeader(Sheet sheet, String header) {
        for(int cellNumber = 0;cellNumber <= sheet.getRow(0).getPhysicalNumberOfCells(); cellNumber++){
            if (sheet.getRow(0).getCell(cellNumber).toString().equalsIgnoreCase(header))
                return cellNumber;
        }
        throw new RuntimeException("No data found for header");
    }

    protected Row readDataRow(Sheet approverDetailsSheet, String approverPosition) {
    Iterator<Row> rowIterator  = approverDetailsSheet.rowIterator();
    while( rowIterator.hasNext()) {
        Row row = rowIterator.next();
        if (row.getCell(0).getStringCellValue().equalsIgnoreCase(approverPosition))
            return row;
    }
        throw new RuntimeException("No data found");
    }

    public ApprovalDetails getApprovalDetails(String approverPosition) {
        createWorkbook(approverDataExcel);
        approverDetailsSheet = workbook.getSheet("Sheet1");

        Row row = readDataRow(approverDetailsSheet, approverPosition);

        String approverDepartment = row.getCell(getCellNumberWithHeader(approverDetailsSheet,"ApproverDepartment")).getStringCellValue();
        String approverDesignation = row.getCell(getCellNumberWithHeader(approverDetailsSheet,"ApproverDesignation")).getStringCellValue();
        String approver = row.getCell(getCellNumberWithHeader(approverDetailsSheet,"Approver")).getStringCellValue();

        return new ApproverDetailsBuilder()
                .withApproverDepartment(approverDepartment)
                .withApproverDesignation(approverDesignation)
                .withApprover(approver)
                .build();
    }

}
