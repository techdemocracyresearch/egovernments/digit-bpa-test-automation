package entities;

public class ApprovalDetails {
    private String approverDepartment;

    private String approverDesignation;

    private String approver;


    public void setApproverDepartment(String approverDepartment) { this.approverDepartment = approverDepartment; }

    public void setApproverDesignation(String approverDesignation) { this.approverDesignation = approverDesignation; }

    public void setApprover(String approver) { this.approver = approver; }

    public String getApproverDepartment() { return approverDepartment;
    }

    public String getApproverDesignation() { return approverDesignation;
    }

    public String getApprover() { return approver;
    }
}
