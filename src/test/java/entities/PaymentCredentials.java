package entities;

public class PaymentCredentials {

    private String paymentGateway;
    private String bankAccountNumber;
    private String bankPayeeName;
    private String cardExpiryDate;
    private String cardCVVNumber;

    public void setPaymentGateway(String paymentGateway){
        this.paymentGateway = paymentGateway;
    }

    public void setBankAccountNumber(String bankAccountNumber) { this.bankAccountNumber = bankAccountNumber; }

    public void setBankPayeeName(String bankPayeeName) { this.bankPayeeName = bankPayeeName;
    }

    public void setCardExpiryDate(String cardExpiryDate) { this.cardExpiryDate = cardExpiryDate; }

    public void setCardCVVNumber(String cardCVVNumber) { this.cardCVVNumber = cardCVVNumber;
    }

    public String getpaymentGateway() { return paymentGateway; }

    public String getBankAccountNumber() { return bankAccountNumber;
    }

    public String getBankPayeeName() { return bankPayeeName;
    }

    public String getCardCVVNumber() { return cardCVVNumber;
    }
}
