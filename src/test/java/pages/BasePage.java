package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;

public class BasePage {
    protected void switchToNewlyOpenedWindow(WebDriver driver) {
        await().atMost(20, SECONDS).until(() -> driver.getWindowHandles().size() > 1);
//        driver.manage().window().maximize();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    protected void selectByVisibleText(WebElement webElement,String value, WebDriver driver){
        await().atMost(20, SECONDS).until(() -> new Select(webElement).getOptions().size() > 1);
        new Select(webElement).selectByVisibleText(value);
    }

    protected void uploadFile(WebElement element, String filePath, WebDriver driver) {
        element.sendKeys(filePath);
    }

    protected void closeWindowsTab(){
        Robot rb = null;
        try {
            rb = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        rb.keyPress(KeyEvent.VK_ALT);
        rb.keyPress(KeyEvent.VK_SPACE);
        rb.keyPress(KeyEvent.VK_C);
    }

    protected void switchToPreviouslyOpenedWindow(WebDriver driver) {
        await().atMost(20, SECONDS).until(() -> driver.getWindowHandles().size() == 1);
//        driver.manage().window().maximize();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    protected void selectByValue(WebElement webElement,String value, WebDriver driver){
        await().atMost(20, SECONDS).until(() -> new Select(webElement).getOptions().size() > 1);
        new Select(webElement).selectByValue(value);
    }

}
