package pages;

import entities.ApprovalDetails;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ApprovalPage extends BasePage {
    WebDriver driver;

    @FindBy(id = "approvalDepartment")
    private WebElement approvalDepartment;

    @FindBy(id = "approvalDesignation")
    private WebElement approvalDesignation;

    @FindBy(id = "approvalPosition")
    private WebElement approver;

    @FindBy(id = "Forward")
    private WebElement Forward;

    @FindBy(id = "buttonApprove")
    private WebElement approveNOC;

    @FindBy(id = "Approve")
    private WebElement approve;

    @FindBy(id = "Revert")
    private WebElement revert;

    @FindBy(id = "approvalComent")
    private WebElement approvalComent;

    public ApprovalPage(WebDriver driver){this.driver = driver;}


    public String getApprover() {
//    approvalDepartment.getText().equalsIgnoreCase(approvalDetails.getApproverDepartment());
//    approvalDesignation.getText().equalsIgnoreCase(approvalDetails.getApproverDesignation());
//    approver.getText().equalsIgnoreCase(approvalDetails.getApprover());
    Forward.click();
    driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    return driver.findElement(By.xpath("//div[@class= 'panel-title text-center no-float']/strong")).getText();
    }

    public void approveNOC() {
        approveNOC.click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public void approve() {
        approve.click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public void reject() {
        approvalComent.sendKeys("Rejected");
        if(driver.findElements(By.id("Initiate Rejection")).size() > 0) {
            driver.findElement(By.id("Initiate Rejection")).click();
        }
        else if(driver.findElements(By.id("Reject")).size() > 0)
        {
            driver.findElement(By.id("Reject")).click();
        }
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public void revert() {
        approvalComent.sendKeys("Reverted");
        revert.click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }
}
