package pages;

import entities.PaymentCredentials;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ByIdOrName;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Properties;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;

public class PermitApplicationPage extends BasePage {
    WebDriver driver;

    @FindBy(linkText = "Plan Scrutiny")
    private WebElement planScrutinyLink;

    @FindBy(linkText = "New Building Plan Scrutiny")
    private WebElement newBuildingPlanScrutinyLink;

    @FindBy(id = "tenantId")
    private WebElement cityName;

    @FindBy(id = "buttonSubmit")
    private WebElement submitButton;

    @FindBy(xpath = "//input[@type='file'][@id = 'myfile']")
    private WebElement dxfUpload;

    @FindBy(id = "applicantName")
    private WebElement applicantName;

    @FindBy(id = "serviceType")
    private WebElement serviceType;

    @FindBy(linkText = "Close")
    private WebElement closeButton;

    @FindBy(linkText = "Building Permission")
    private WebElement buildingPermissionLink;

    @FindBy(linkText = "New Construction")
    private WebElement newConstructionLink;

    @FindBy(id = "eDcrNumber")
    private WebElement eDcrNumber;

    @FindBy(id = "Residential")
    private WebElement residentialOccupancy;

    @FindBy(css = "div.bootstrap-tagsinput > input[type=\"text\"]")
    private WebElement buildingApplicantName;

    @FindBy(id = "address")
    private WebElement applicantAddress;

    @FindBy(id = "mobileNumber")
    private WebElement applicantMobileNumber;

    @FindBy(id = "gender")
    private WebElement applicantgender;

    @FindBy(id = "REVENUECircle")
    private WebElement selectCircle;

    @FindBy(id = "REVENUERevenueWard")
    private WebElement selectWard;

    @FindBy(xpath = "//div[@class = 'panel panel-primary'][4]/div/div[@class = 'panel-title']")
    private WebElement detailsOfPlotSection;

    @FindBy(xpath = "//div[@id='appliccation-info']/div[6]/div[2]/div[3]/div[2]/div/input")
    private WebElement natureofOwnership;

    @FindBy(id = "khataNumber")
    private WebElement khataNumber;

    @FindBy(id = "holdingNumber")
    private WebElement holdingNumber;

    @FindBy(id = "mspPlotNumber")
    private WebElement mspPlotNumber;

    @FindBy(id = "constructionCost")
    private WebElement constructionCost;

    @FindBy(id = "architectAccepted")
    private WebElement architectAccepted;

    @FindBy(partialLinkText = "Document Details")
    private WebElement documentDetailsTab;

    @FindBy(xpath = "//div[@class = 'files-upload-container mandatory-dcr-doc']/input")
    private WebElement floorPlan;

    @FindBy(xpath = "//div[@class = 'files-upload-container mandatory-dcr-doc']/input[@name = 'permitDcrDocuments[1].dcrDocument.files']")
    private WebElement servicePlan;

    @FindBy(xpath = "//div[@class = 'files-upload-container mandatory-dcr-doc']/input[@name = 'permitDcrDocuments[2].dcrDocument.files']")
    private WebElement sitePLan;

    @FindBy(xpath = "//div[@class = 'files-upload-container ']/input[@name = 'permitDocuments[0].document.files']")
    private WebElement technicalPersonDeclaration;

    @FindBy(xpath = "//div[@class = 'files-upload-container ']/input[@name = 'permitDocuments[1].document.files']")
    private WebElement citizenDeclaration;

    @FindBy(xpath = "//div[@class = 'files-upload-container ']/input[@name = 'permitDocuments[2].document.files']")
    private WebElement landTaxReceipt;

    @FindBy(xpath = "//div[@class = 'files-upload-container ']/input[@name = 'permitDocuments[3].document.files']")
    private WebElement possessionCertificate;

    @FindBy(xpath = "//div[@class = 'files-upload-container ']/input[@name = 'permitDocuments[4].document.files']")
    private WebElement titleDeed;

    @FindBy(xpath = "//div[@class = 'files-upload-container _MOEF_environment_clearance']/input[@name = 'permitNocDocuments[0].nocDocument.files']")
    private WebElement MOEFNOC;

    @FindBy(xpath = "//div[@class = 'files-upload-container _NOC_from_Airport_Authority']/input[@name = 'permitNocDocuments[1].nocDocument.files']")
    private WebElement AAINOC;

    @FindBy(xpath = "//div[@class = 'files-upload-container _NOC_from_Fire_Authority']/input[@name = 'permitNocDocuments[2].nocDocument.files']")
    private WebElement FireNOC;

    @FindBy(xpath = "//div[@class = 'files-upload-container _NOC_from_Heritage_Conservation_Committee_National_Monuments_Authority']/input[@name = 'permitNocDocuments[3].nocDocument.files']")
    private WebElement NMANOC;

    @FindBy(xpath = "//div[@class = 'files-upload-container _NOC_from_Irrigation_Department']/input[@name = 'permitNocDocuments[4].nocDocument.files']")
    private WebElement IrrigationNOC;

    @FindBy(name = "button2")
    private WebElement applicationCloseButton;

    @FindBy(id = "citizenAccepted")
    private WebElement citizenAcceptCheckBox;

    @FindBy(id = "buttonAccept")
    private WebElement citizenAcceptButton;

    @FindBy(partialLinkText = "Print Permit Order")
    private WebElement printPermitOrder;

    @FindBy(partialLinkText = "New Appointment")
    private WebElement initiateInspection;

    @FindBy(xpath = "//input[@type = 'file']")
    private WebElement fireDeptNOC;

    @FindBy(id = "ocEDcrNumber")
    private WebElement ocEDcrNumber;

    public PermitApplicationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnPlanScrutiny(String planScrutiny) {
        planScrutinyLink.click();
        driver.findElement(By.linkText(planScrutiny)).click();
//        newBuildingPlanScrutinyLink.click();
        switchToNewlyOpenedWindow(driver);
    }

    public void selectCity(String city) {
        selectByVisibleText(cityName, city, driver);
        submitButton.click();

    }

    public void enterDCRDetails(String riskType) {
        String filePath;
        switch (riskType) {

            case "LowRiskType":
            filePath = System.getProperty("user.dir") + "/src/test/resources/dxfFile/Low_Accepted_MAIN_Setback.dxf";
            uploadFile(dxfUpload, filePath, driver);
            break;

            case "HighRiskType":
                filePath = System.getProperty("user.dir") + "/src/test/resources/dxfFile/HIGH_ACCEPTED_FIRE_NOC.dxf";
                uploadFile(dxfUpload, filePath, driver);
                break;

            case "MediumRiskType":
                filePath = System.getProperty("user.dir") + "/src/test/resources/dxfFile/HIGH_ACCEPTED_FIRE_NOC.dxf";
                uploadFile(dxfUpload, filePath, driver);
                break;
        }

    }

    public String scrutinyNumber() {
        String scrutinyNumber = driver.findElement(By.xpath("//td[2]")).getText();
        closeButton.click();
        switchToPreviouslyOpenedWindow(driver);
        return scrutinyNumber;
    }

    public void clickOnNewConstruction(String applicationType) {
        buildingPermissionLink.click();
        driver.findElement(By.linkText(applicationType)).click();
//        newConstructionLink.click();
        switchToNewlyOpenedWindow(driver);
    }

    public void basicScrutinyDetails(String scrutinyNumber) {
        driver.navigate().refresh();
        eDcrNumber.sendKeys(scrutinyNumber);
        eDcrNumber.sendKeys(Keys.TAB);
    }

    public void blockWiseOccupancy() {
        Select select = new Select(residentialOccupancy);
        select.selectByVisibleText("Row Housing");
    }

    public void applicantDetails() {
        buildingApplicantName.sendKeys("Akhila");
        applicantAddress.sendKeys("Bangalore");
        applicantMobileNumber.sendKeys("2828282828");
        selectByValue(applicantgender, "FEMALE", driver);
    }

    public void boundaryDetails() {
        driver.findElement(By.cssSelector("div.panel-heading.toggle-header.custom_form_panel_heading")).click();
        selectByVisibleText(selectCircle, "Circle 1", driver);
        selectByVisibleText(selectWard, "Ward 1", driver);
    }

    public void plotDetails() {
        detailsOfPlotSection.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(natureofOwnership));
        natureofOwnership.sendKeys("OWN");
        khataNumber.sendKeys("ab/12");
        holdingNumber.sendKeys("1234");
        mspPlotNumber.sendKeys("1234");
        constructionCost.sendKeys("10000");
//        architectAccepted.click();
    }

    public void architectDeclaration() {
        architectAccepted.click();
    }

    public void attachDocuments() {

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[2]/a")));
        String filePath = System.getProperty("user.dir") + "/src/test/resources/documentList/Document.pdf";
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDcrDocuments[0].dcrDocument.files']/i")));
        uploadFile(floorPlan, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDcrDocuments[1].dcrDocument.files']/i")));
        uploadFile(servicePlan, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDcrDocuments[2].dcrDocument.files']/i")));
        uploadFile(sitePLan, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDocuments[0].document.files']/i")));
        uploadFile(technicalPersonDeclaration, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDocuments[1].document.files']/i")));
        uploadFile(citizenDeclaration, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDocuments[2].document.files']/i")));
        uploadFile(landTaxReceipt, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDocuments[3].document.files']/i")));
        uploadFile(possessionCertificate, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitDocuments[4].document.files']/i")));
        uploadFile(titleDeed, filePath, driver);
        closeWindowsTab();
    }

    public void attachNOC() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[3]/a")));
        String filePath = System.getProperty("user.dir") + "/src/test/resources/documentList/Document.pdf";
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitNocDocuments[0].nocDocument.files']/i")));
        uploadFile(MOEFNOC, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitNocDocuments[1].nocDocument.files']/i")));
        uploadFile(AAINOC, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitNocDocuments[2].nocDocument.files']/i")));
        uploadFile(FireNOC, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitNocDocuments[3].nocDocument.files']/i")));
        uploadFile(NMANOC, filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'permitNocDocuments[4].nocDocument.files']/i")));
        uploadFile(IrrigationNOC, filePath, driver);
        closeWindowsTab();
    }

    public void sendToCitizen() {
        driver.findElement(By.id("bpaSend")).click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public String copyApplicationNumber() {
        String msg = driver.findElement(By.xpath("//div[@class = 'panel-title text-center no-float']/strong")).getText();
        String applicationNumber = msg.split("\\s")[9].substring(1, 13);
        applicationCloseButton.click();
        switchToPreviouslyOpenedWindow(driver);
        return applicationNumber;
    }

    public void citizenAccept() {
        citizenAcceptCheckBox.click();
        citizenAcceptButton.click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
        applicationCloseButton.click();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void submitPermitApplication() {
        driver.findElement(By.id("buttonSubmit")).click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public void enterPaymentCredentials(PaymentCredentials paymentCredentials) {
        for (int i = 1; i <= driver.findElements(By.xpath("//td[@class = 'blueborderfortd']/span")).size(); i++) {
            if (driver.findElement(By.xpath("//td[@class = 'blueborderfortd']/span[" + i + "]")).getText().equalsIgnoreCase(paymentCredentials.getpaymentGateway()));
            driver.manage().window().maximize();
            driver.findElement(By.xpath("//td[@class = 'blueborderfortd']/input[@type ='radio']")).click();
        }
        driver.findElement(By.id("checkbox")).click();
        driver.findElement(By.id("button2")).click();
        driver.findElement(By.id("ccard_number")).sendKeys(paymentCredentials.getBankAccountNumber());
        driver.findElement(By.id("cname_on_card")).sendKeys(paymentCredentials.getBankPayeeName());
        driver.findElement(By.id("ccvv_number")).sendKeys(paymentCredentials.getCardCVVNumber());
        selectByValue(driver.findElement(By.id("cexpiry_date_month")),"01",driver);
//        driver.findElement(By.id("cexpiry_date_month")).sendKeys("01");
        selectByValue(driver.findElement(By.id("cexpiry_date_year")),"2022",driver);
//        driver.findElement(By.id("cexpiry_date_year")).sendKeys("2022");
        driver.findElement(By.id("pay_button")).click();
        WebDriverWait wait = new WebDriverWait(driver, Properties.waitTime);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@type = 'submit']"))));
        driver.findElement(By.xpath("//input[@type = 'submit']")).click();
    }

    public void verifyGenerateReceipt() {
        driver.findElement(By.linkText("Generate Receipt")).isDisplayed();
        driver.close();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void printPermitOrder() {
        printPermitOrder.click();
        switchToNewlyOpenedWindow(driver);
        driver.close();
        switchToNewlyOpenedWindow(driver);
        applicationCloseButton.click();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void closeAcknowledgement() {
        applicationCloseButton.click();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void initiateInspectionAppointment() {
        initiateInspection.click();
        driver.findElement(By.id("appointmentDate")).sendKeys("31/05/2019");
        driver.findElement(By.id("appointmentDate")).sendKeys(Keys.TAB);
        driver.findElement(By.xpath("//div[@class = 'input-group date']/input")).sendKeys("11:00 AM");
        driver.findElement(By.xpath("//div[@class = 'input-group date']/input")).sendKeys(Keys.TAB);
        driver.findElement(By.id("scheduleSubmit")).click();
        Boolean scheduleVerify = driver.findElement(By.xpath("//div[@class = 'panel-title']")).getText().contains("Scheduled Appointment Details");
        Assert.assertTrue(scheduleVerify);
        closeButton.click();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void captureInspectionDetails() {
        driver.findElement(By.partialLinkText("Capture Inspection Details")).click();
        switchToNewlyOpenedWindow(driver);
        submitButton.click();
        WebDriverWait wait = new WebDriverWait(driver, Properties.waitTime);
        wait.until(ExpectedConditions.visibilityOf(applicationCloseButton));
        applicationCloseButton.click();
        switchToNewlyOpenedWindow(driver);
    }

    public void initiateNOC() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[3]/a")));
        driver.findElement(By.id("btninitiatenoc")).click();
        switchToNewlyOpenedWindow(driver);
        switchToNewlyOpenedWindow(driver);
//        String NOCAppNum = driver.findElement(By.xpath("//div[@class = 'panel-title text-center no-float']/strong")).getText().split("\\s")[9].substring(1,21);
        applicationCloseButton.click();
        switchToNewlyOpenedWindow(driver);
//        return NOCAppNum;
    }

    public void attachFireNOC() {
        String filePath = System.getProperty("user.dir") + "/src/test/resources/documentList/Document.pdf";
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'bpaNocApplication.files']/i")));
        uploadFile(fireDeptNOC, filePath, driver);
    }

    public void updateNOCStatus() {
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'permitNocDocuments[0].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'permitNocDocuments[1].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'permitNocDocuments[2].nocDocument.nocStatus']")),"Approved",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'permitNocDocuments[3].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'permitNocDocuments[4].nocDocument.nocStatus']")),"Already Available",driver);
    }

    public void selectPermitCondition() {
        driver.findElement(By.id("staticPermitConditionsTemp0.noticeCondition.required1")).click();
    }

    public void clickOnPayOnline() {
        driver.findElement(By.xpath("//button[@class='btn btn-primary'][@type='button']")).click();
        driver.findElement(By.partialLinkText("Pay Fee Online")).click();
    }

    public void generatePermitOrder() {
        driver.findElement(By.id("Generate Permit Order")).click();
        driver.findElement(By.xpath("//button[@class='btn btn-primary'][@type='button']")).click();
        driver.close();
        switchToPreviouslyOpenedWindow(driver);
    }

    public void servicesComplted() {
        driver.findElement(By.xpath("//div[@id = 'totalServicesCompletedSize']")).click();
        driver.findElement(By.xpath("//div[@id = 'totalServicesCompletedSize']")).click();
    }

    public String getPermitNumber() {
        String permitNumber = driver.findElements(By.xpath("//div[@class = 'panel-body'][1]/div/div[4]")).get(0).getText();
        return permitNumber;
    }

    public void planScrutinyApplicantDetails() {
        applicantName.sendKeys("Akhila Vasanth");
        selectByVisibleText(serviceType, "New Construction", driver);
    }

    public void submitPlanScrutiny() {
        submitButton.click();
        driver.findElement(By.xpath("//button[@class='btn btn-primary'][@type='button']")).click();
    }

    public void enterPermitNumber(String permitNumber) {
     driver.findElement(By.id("planPermitNumber")).sendKeys(permitNumber);
        driver.findElement(By.id("planPermitNumber")).sendKeys(Keys.TAB);
    }

    public void OCPlanScrutiny(String scrutinyNumber) {
        ocEDcrNumber.sendKeys(scrutinyNumber);
        ocEDcrNumber.sendKeys(Keys.TAB);
        driver.findElement(By.id("commencedDate")).sendKeys("01/06/2019");
        driver.findElement(By.id("completionDate")).sendKeys("31/06/2019");
    }

    public void submitOCApplication() {
        driver.findElement(By.id("ocSubmit")).click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
    }

    public void attachOCDocuments() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[2]/a")));
        String filePath = System.getProperty("user.dir") + "/src/test/resources/documentList/Document.pdf";
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'documents[0].document.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'documents[0].document.files']")), filePath, driver);
        closeWindowsTab();
    }

    public String copyOCApplicationNumber() {
        String OCapplicationNumber = driver.findElement(By.tagName("strong")).getText().split("\\s")[4];
        applicationCloseButton.click();
        switchToPreviouslyOpenedWindow(driver);
        return OCapplicationNumber;
    }

    public void attachOCNOCDocuments() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[3]/a")));
        String filePath = System.getProperty("user.dir") + "/src/test/resources/documentList/Document.pdf";
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'nocDocuments[0].nocDocument.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'nocDocuments[0].nocDocument.files']")), filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'nocDocuments[1].nocDocument.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'nocDocuments[1].nocDocument.files']")), filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'nocDocuments[2].nocDocument.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'nocDocuments[2].nocDocument.files']")), filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'nocDocuments[3].nocDocument.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'nocDocuments[3].nocDocument.files']")), filePath, driver);
        closeWindowsTab();
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[@data-file-input-name = 'nocDocuments[4].nocDocument.files']/i")));
        uploadFile(driver.findElement(By.xpath("//div[@class = 'files-upload-container']/input[@name = 'nocDocuments[4].nocDocument.files']")), filePath, driver);
        closeWindowsTab();

    }

    public void updateOCNOCStatus() {
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'nocDocuments[0].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'nocDocuments[1].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'nocDocuments[2].nocDocument.nocStatus']")),"Approved",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'nocDocuments[3].nocDocument.nocStatus']")),"Already Available",driver);
        selectByVisibleText(driver.findElement(By.xpath("//select[@name = 'nocDocuments[4].nocDocument.nocStatus']")),"Already Available",driver);
    }

    public void generateOC() {
        driver.findElement(By.id("Generate Occupancy Certificate")).click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
        driver.close();
        switchToPreviouslyOpenedWindow(driver);
    }

    public String copyInspectionAppNum() {
        String InspectAppNum = driver.findElement(By.xpath("//div[@class = 'panel-title text-center no-float']/strong")).getText().split("\\s")[2].substring(0,16);
        return InspectAppNum;
    }

    public void enterPermitNumberForInspection(String permitNumber) {
        driver.findElement(By.id("planPermissionNumber")).sendKeys(permitNumber);
        driver.findElement(By.id("planPermissionNumber")).sendKeys(Keys.TAB);
    }

    public void rejectionReason() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//ul[@id = 'settingstab']/li[1]/a")));
        driver.findElement(By.xpath("//div[@class = 'panel panel-primary'][7]/div/div")).click();
        driver.findElement(By.cssSelector("input[data-change-to = 'rejectionReasonsTemp[0].noticeCondition.required']")).click();
    }

    public void generateRejectionNotice() {
        driver.findElement(By.id("Generate Rejection Notice")).click();
        driver.findElement(By.xpath("//button[@class = 'btn btn-primary'][@data-bb-handler = 'confirm']")).click();
        driver.close();
        switchToPreviouslyOpenedWindow(driver);
    }
}
