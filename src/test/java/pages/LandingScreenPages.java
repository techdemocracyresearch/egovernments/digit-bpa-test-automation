package pages;

import entities.LoginDetails;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.concurrent.TimeUnit.SECONDS;

public class LandingScreenPages extends BasePage{
    WebDriver driver;
    @FindBy(id = "j_username")
    private WebElement usernameTextBox;

    @FindBy(id = "j_password")
    private WebElement passwordTextBox;

    @FindBy(id = "signin-action")
    private WebElement signInLink;

    @FindBy(xpath = "//i[@class = 'fa fa-sign-out']")
    private WebElement signOutLink;

    @FindBy(xpath = "//span[@class= 'text hidden-sm']")
    private WebElement citizenProfileLink;

    @FindBy(xpath = "//a[@class = 'profile-name']")
    private WebElement employeeProfileLink;

    @FindBy(xpath = "//li[@class = 'hidden-xs menu-responsive']/ul/li[6]/a/span")
    private WebElement employeeSignoutLink;

    public LandingScreenPages(WebDriver driver){this.driver = driver;}
    public void login(LoginDetails loginDetails) {
    usernameTextBox.sendKeys(loginDetails.getLoginId());
    passwordTextBox.sendKeys(loginDetails.getPassword());
    signInLink.click();
    }

    public void logout() {
    citizenProfileLink.click();
    signOutLink.click();
    }

    public void openApplicationInCitizenInbox(String applicationNumber) {
        driver.findElement(By.xpath(".//*[@id='bpa-home-table']/tbody[1]/tr[1]/td[2]")).click();
        switchToNewlyOpenedWindow(driver);
    }

    public WebElement getApplication(String applicationNumber) {
        List<WebElement> totalrows;
        await().atMost(30, SECONDS).until(() -> driver.findElements(By.cssSelector("[id='official_inbox'] tr td")).size() > 1);
        totalrows = driver.findElement(By.id("official_inbox")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
        for(WebElement applicationrow : totalrows){
            if(applicationrow.findElements(By.tagName("td")).get(4).findElement(By.tagName("span")).getAttribute("data-text").contains(applicationNumber))
                return applicationrow;
        }
        throw new RuntimeException("No application row found in Inbox -- " + applicationNumber);
    }

    public void openApplicationInEmployeeInbox(String applicationNumber){
        WebElement element = getApplication(applicationNumber);
        element.click();
        switchToNewlyOpenedWindow(driver);
        driver.navigate().refresh();
    }

    public void employeeLogout() {
    employeeProfileLink.click();
    employeeSignoutLink.click();
    }

    public void openInspectionApplicationInEmployee(String applicationNumber) {
       WebElement element = getInspectionApplication(applicationNumber);
       element.click();
       switchToNewlyOpenedWindow(driver);
       driver.navigate().refresh();
    }

    private WebElement getInspectionApplication(String applicationNumber) {
        List<WebElement> totalrows;
        await().atMost(30, SECONDS).until(() -> driver.findElements(By.cssSelector("[id='official_inbox'] tr td")).size() > 1);
        totalrows = driver.findElement(By.id("official_inbox")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
        for(WebElement applicationrow : totalrows){
            if(applicationrow.findElements(By.tagName("td")).get(4).getText().contains(applicationNumber));
            return applicationrow;
        }
        throw new RuntimeException("No application row found in Inbox -- " + applicationNumber);
    }
}
