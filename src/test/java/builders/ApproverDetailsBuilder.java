package builders;

import entities.ApprovalDetails;

public class ApproverDetailsBuilder {

    ApprovalDetails approvalDetails = new ApprovalDetails();

    public ApproverDetailsBuilder withApproverDepartment(String approverDepartment) {
        approvalDetails.setApproverDepartment(approverDepartment);
        return this;
    }

    public ApproverDetailsBuilder withApproverDesignation(String approverDesignation) {
        approvalDetails.setApproverDesignation(approverDesignation);
        return this;
    }

    public ApproverDetailsBuilder withApprover(String approver) {
        approvalDetails.setApprover(approver);
        return this;
    }

    public ApprovalDetails build() { return approvalDetails; }
}
