package builders;

import entities.PaymentCredentials;

public class PaymentCredentialsBuilder {

    PaymentCredentials paymentCredentials = new PaymentCredentials();

    public PaymentCredentialsBuilder withPaymentGateway(String paymentGateway) {
        paymentCredentials.setPaymentGateway(paymentGateway);
        return this;
    }

    public PaymentCredentialsBuilder withBankAccountNumber(String bankAccountNumber) {
        paymentCredentials.setBankAccountNumber(bankAccountNumber);
        return this;
    }

    public PaymentCredentialsBuilder withBankPayeeName(String bankPayeeName) {
        paymentCredentials.setBankPayeeName(bankPayeeName);
        return this;
    }


    public PaymentCredentialsBuilder withCardExpiryDate(String cardExpiryDate) {
        paymentCredentials.setCardExpiryDate(cardExpiryDate);
        return this;
    }

    public PaymentCredentialsBuilder withCardCVVNumber(String cardCVVNumber) {
        paymentCredentials.setCardCVVNumber(cardCVVNumber);
        return this;
    }

    public PaymentCredentials build() { return paymentCredentials;
    }
}
