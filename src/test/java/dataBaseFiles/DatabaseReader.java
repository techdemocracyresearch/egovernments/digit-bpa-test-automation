package dataBaseFiles;

import builders.LoginDetailsBuilder;
import builders.PaymentCredentialsBuilder;
import cucumber.api.java8.En;
import entities.LoginDetails;
import entities.PaymentCredentials;
import steps.BaseSteps;
import steps.PageStore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static steps.BaseSteps.pageStore;

public class DatabaseReader extends BaseSteps implements En {


    public LoginDetails getLoginDetails(String currentUser) throws SQLException {
        String dbquery = "select * from eg_kk_logintestdata";
        Statement stmt = pageStore.dbConnection().createStatement();
        ResultSet rs = stmt.executeQuery(dbquery);
        if (rs== null)
        {
            System.out.println("No data found");
        }
        String id = null;
        String password = null;
        while (rs.next())
        {
            if(rs.getString("positions").equalsIgnoreCase(currentUser)) {
                id = rs.getString("userid");
                password = rs.getString("password");
            }
        }
        return new LoginDetailsBuilder().withLoginId(id).withPassword(password)
//                .withHasZone(hasZone)
                .build();
    }

    public PaymentCredentials getPaymentCredentials(String bank) throws SQLException {
        String dbquery = "Select * from eg_online_payment";
        Statement stmt = pageStore.dbConnection().createStatement();
        ResultSet rs = stmt.executeQuery(dbquery);
        if (rs== null)
        {
            System.out.println("No data found");
        }
        String paymentGateway = null;
        String bankAccountNumber = null;
        String bankPayeeName = null;
        String cardExpiryDate = null;
        String cardCVVNumber = null;
        while (rs.next())
        {
            if(rs.getString("bank").equalsIgnoreCase(bank)) {
                paymentGateway = rs.getString("paymentGateway");
                bankAccountNumber = rs.getString("bankAccountNumber");
                bankPayeeName = rs.getString("bankPayeeName");
                cardExpiryDate = rs.getString("cardExpiryDate");
                cardCVVNumber = rs.getString("cardCVVNumber");
            }
        }

        return new PaymentCredentialsBuilder()
                .withPaymentGateway(paymentGateway)
                .withBankAccountNumber(bankAccountNumber)
                .withBankPayeeName(bankPayeeName)
                .withCardExpiryDate(cardExpiryDate)
                .withCardCVVNumber(cardCVVNumber)
                .build();
    }
}
