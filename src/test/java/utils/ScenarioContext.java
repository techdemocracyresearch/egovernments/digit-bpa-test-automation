package utils;

import java.io.Serializable;

public class ScenarioContext implements Serializable {
    private String scrutinyNumber;
    private String applicationNumber;
    private String successMsessage;
    private String NOCApplicationNumber;
    private String permitNumber;
    private String InspectionAppNum;

    public void setNOCApplocationNumbet(String NOCApplicationNumber) { this.NOCApplicationNumber = NOCApplicationNumber;
    }

    public void setScrutinyNumber(String scrutinyNumber) { this.scrutinyNumber = scrutinyNumber; }

    public String getScrutinyNumber() { return scrutinyNumber;}

    public void setApplicationNumber(String applicationNumber) { this.applicationNumber = applicationNumber;
    }

    public String getApplicationNumber() {
       return applicationNumber;
    }

    public void setSuccessMsessage(String successMsessage) { this.successMsessage = successMsessage;
    }

    public String getSuccessMessage() { return successMsessage;
    }

    public String getNOCApplicationNumber() { return NOCApplicationNumber;
    }

    public void setPermitNumber(String permitNumber) { this.permitNumber = permitNumber;
    }

    public String getPermitNumber() { return permitNumber;
    }

    public void setInspectionAppNum(String InspectionAppNum) { this.InspectionAppNum = InspectionAppNum;
    }
}
