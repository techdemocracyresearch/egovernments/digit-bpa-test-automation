Feature: Stakeholder apply for permit of a building on behalf of citizen

  Scenario: Stakeholder apply for low risk permit of a building on behalf of citizen

    # Architect verifies the plan scrutiny

    Given Architect logs in
    And he apply for New Building Plan Scrutiny Plan Scrutiny under Plan Scrutiny
#    And he selects required city as "Sonpur"
    And he enters dcr details for "LowRiskType"
    And he enters applicant details for plan scrutiny
    And he submit plan scrutiny
    And he copies the scrutiny number

    # Architect apply for permit order
    When he apply for New Construction building permit
    And he enters basic scrutiny details
    And he enters Block wise occupancy details
    And he enters Applicant Details
    And he selects boundary details
    And he enters plot details
    And he declares that above information provided is correct
    And he attach the required documents
    And he attach the required NOC documnets
    And he send the application to citizen
    And he copy application number
    And current user logs out

    When citizen logs in
    And he opens the application from his inbox
    And he accepts the building plan application
    And current user logs out


    When Architect logs in
    And he opens the application from his inbox
    And he submit the application
    And he pay permit scrutiny fee for "HDFC"
    And he verifies collection receipt has been generated
    And he opens the application from his inbox
    And he print the permit order
    And current user logs out

    When SectionClerk logs in
    And he choose to act upon the above application
#    And he initiate mandatory NOC
    And he forward the application to "Building_OverSeer"
    And he closes the acknowledgement page
    And current employee logs out

    When BuildingOverseer logs in
    And he choose to act upon the above application
    And he initiate inspection appointment
    And he choose to act upon the above application
    And he capture the inspection details
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he forward the application to "Superintendent"
    And he closes the acknowledgement page
    And current employee logs out

    When Superintendent logs in
    And he choose to act upon the above application
    And he update NOC status
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he approve the application
    And he closes the acknowledgement page
    And current employee logs out


  Scenario: Stakeholder apply for high risk permit of a building on behalf of citizen

     Given Architect logs in
    And he apply for New Building Plan Scrutiny Plan Scrutiny under Plan Scrutiny
#    And he selects required city as "Sonpur"
    And he enters dcr details for "HighRiskType"
    And he enters applicant details for plan scrutiny
    And he submit plan scrutiny
    And he copies the scrutiny number

    # Architect apply for permit order
    When he apply for New Construction building permit
    And he enters basic scrutiny details
    And he enters Block wise occupancy details
    And he enters Applicant Details
    And he selects boundary details
    And he enters plot details
    And he declares that above information provided is correct
    And he attach the required documents
    And he attach the required NOC documnets
    And he send the application to citizen
    And he copy application number
    And current user logs out


     When citizen logs in
     And he opens the application from his inbox
     And he accepts the building plan application
     And current user logs out


     When Architect logs in
     And he opens the application from his inbox
     And he submit the application
     And he pay permit scrutiny fee for "HDFC"
     And he verifies collection receipt has been generated
#     And he opens the application from his inbox
#     And he print the permit order
     And current user logs out


     When SectionClerk logs in
     And he choose to act upon the above application
#     And he initiate mandatory NOC
     And he forward the application to "Building_OverSeer"
     And he closes the acknowledgement page
     And current employee logs out

#     When FireOfficial logs in
#     And he opens the application from his inbox
#     And he attach the noc for fire
#     And he approve noc application
#     And he closes the acknowledgement page
#     And current user logs out

     When BuildingOverseer logs in
     And he choose to act upon the above application
     And he initiate inspection appointment
     And he choose to act upon the above application
     And he capture the inspection details
     And he forward the application to "AssistantEngineer"
     And he closes the acknowledgement page
     And current employee logs out

     When AssistantEngineer logs in
     And he choose to act upon the above application
     And he forward the application to "Superintendent"
     And he closes the acknowledgement page
     And current employee logs out

     When Superintendent logs in
     And he choose to act upon the above application
     And he update NOC status
     And he forward the application to "AssistantEngineer"
     And he closes the acknowledgement page
     And current employee logs out

     When AssistantEngineer logs in
     And he choose to act upon the above application
     And he forward the application to "AssistantExecutiveEngineer"
#     And he approve the permit application
     And he closes the acknowledgement page
     And current employee logs out

     When AssistantExecutiveEngineer logs in
     And he choose to act upon the above application
     And he forward the application to "ExecutiveEngineer"
#     And he approve the permit application
     And he closes the acknowledgement page
     And current employee logs out

     When ExecutiveEngineer logs in
     And he choose to act upon the above application
     And he selects the permit conditions
     And he approve the application
     And he closes the acknowledgement page
     And current employee logs out


     When citizen logs in
     And he opens the application from his inbox
     And choose to pay online
     And he pay permit scrutiny fee for "HDFC"
     And he verifies collection receipt has been generated
     And current user logs out

     When Superintendent logs in
     And he choose to act upon the above application
     And he forward the application to "AssistantEngineer"
     And he closes the acknowledgement page
     And current employee logs out

     When ExecutiveEngineer logs in
     And he choose to act upon the above application
     And he generate permit order
     And current employee logs out

     When Architect logs in
     And he click on Services Completed tab
     And he opens the application from his inbox
     And he print the permit order
     And current user logs out