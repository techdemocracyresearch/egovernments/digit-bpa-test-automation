Feature: Low Risk Application flow

  Scenario: Apply for occupancy certificate for low risk building

    Given Architect logs in
    And he apply for New Building Plan Scrutiny under Plan Scrutiny
#    And he selects required city as "Sonpur"
    And he enters dcr details for "LowRiskType"
    And he enters applicant details for plan scrutiny
    And he submit plan scrutiny
    And he copies the scrutiny number

    # Architect apply for permit order
    When he apply for New Construction building permit
    And he enters basic scrutiny details
    And he enters Block wise occupancy details
    And he enters Applicant Details
    And he selects boundary details
    And he enters plot details
    And he declares that above information provided is correct
    And he attach the required documents
    And he attach the required NOC documnets
    And he send the application to citizen
    And he copy application number
    And current user logs out

    When citizen logs in
    And he opens the application from his inbox
    And he accepts the building plan application
    And current user logs out


    When Architect logs in
    And he opens the application from his inbox
    And he submit the application
    And he pay permit scrutiny fee for "HDFC"
    And he verifies collection receipt has been generated
    And he opens the application from his inbox
    And he print the permit order
    And current user logs out

    When SectionClerk logs in
    And he choose to act upon the above application
#    And he initiate mandatory NOC
    And he forward the application to "Building_OverSeer"
    And he closes the acknowledgement page
    And current employee logs out

    When BuildingOverseer logs in
    And he choose to act upon the above application
    And he initiate inspection appointment
    And he choose to act upon the above application
    And he capture the inspection details
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he forward the application to "Superintendent"
    And he closes the acknowledgement page
    And current employee logs out

    When Superintendent logs in
    And he choose to act upon the above application
    And he update NOC status
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he approve the application
    And he copies the building permit number
    And he closes the acknowledgement page
    And current employee logs out

    When Architect logs in
    And he apply for New Occupancy Certificate Plan Scrutiny under Plan Scrutiny
    And he enters dcr details for "LowRiskType"
    And he enters permit number
    And he submit plan scrutiny
    And he copies the scrutiny number

    When he apply for Apply For Occupancy Certificate building permit
    And he enters occupancy certificate scrutiny number
    And he attach the required documents for OC
    And he attach the required NOC documents for OC
    And he submit oc application
    And he copies the Occupancy Application Number
    And current user logs out

    When SectionClerk logs in
    And he choose to act upon the above application
#    And he initiate mandatory NOC
    And he forward the application to "Building_OverSeer"
    And he closes the acknowledgement page
    And current employee logs out

    When BuildingOverseer logs in
    And he choose to act upon the above application
    And he initiate inspection appointment
    And he choose to act upon the above application
    And he capture the inspection details
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he forward the application to "Superintendent"
    And he closes the acknowledgement page
    And current employee logs out

    When Superintendent logs in
    And he choose to act upon the above application
    And he update OCNOC status
    And he forward the application to "AssistantEngineer"
    And he closes the acknowledgement page
    And current employee logs out

    When AssistantEngineer logs in
    And he choose to act upon the above application
    And he approve the application
    And he copies the building permit number
    And he closes the acknowledgement page
    And current employee logs out
